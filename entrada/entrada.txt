### Ejemplo de como redactar una red para el problema.

#

### Los comentarios empiezan con '#' y se ignorarán al momento de procesar 
### el archivo de texto, así como las lineas vacías.

### Importante, si hay repeticiones de aristas o vértices
### se quedará con la primer definición e estos.


## Lista de Vertices de Suministro y Demanda.
## No es necesario poner t* y s*, el programa ya hace ese paso.

# ID_ARISTA TIPO VALOR 
#   - TIPO puede ser "S" (Suministro) o "D" (Demanda)

0 S -23
5 D 23

## Lista de Aristas ##
# ID__VERTICE_1 DIRECCION ID_VERTICE_2 FLUJO_MAX 
#   -DIRECCION se representa con "->" (del vertice 1 al vertice 2)

0 -> 1 16
0 -> 2 13
1 -> 2 10
1 -> 3 12
2 -> 1 4
2 -> 4 14
3 -> 2 9
3 -> 5 20
4 -> 3 7
4 -> 5 4

# 