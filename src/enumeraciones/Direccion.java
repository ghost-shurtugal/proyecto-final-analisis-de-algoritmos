package enumeraciones;

/**
 * Clase de enumeración que tiene el tipo de direcciones de una arista de la
 * red.
 *
 * Originalmente se podía poner '<-' o '<->' en el archivo de entrada, pero por
 * cuestiones de tiempo se dejó sólo el '->'.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public enum Direccion {
    DE_V1_A_V2
}
