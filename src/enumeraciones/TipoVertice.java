package enumeraciones;

/**
 * Clase de enumaración que nos permite definir el tipo de vertice de la red.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public enum TipoVertice {
    NORMAL,
    SUMINISTRO,
    DEMANDA,
    SUMINISTRO_ESTRELLA,
    DEMANDA_ESTRELLA
}
