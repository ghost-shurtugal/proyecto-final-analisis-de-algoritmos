/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras;

import enumeraciones.Direccion;
import java.util.Objects;

/**
 * Estructura que implementa a un vértice de una gráfica.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public class Arista {

    private float flujoMaximo, flujoActual;

    public final Vertice VERTICE1, VERTICE2;

    public final Direccion DIRECCION;

    private boolean residual;

    /**
     * Constructor básico para una arista.
     *
     * @param flujoMaximo Es el flujo máximo que puede pasar por la arista.
     * @param VERTICE1 Es el primer vértice de la arista.
     * @param VERTICE2 Es el segundo vértice de la arista.
     * @param DIRECCION Es la dirección que tiene la arista.
     */
    public Arista(float flujoMaximo, Vertice VERTICE1, Vertice VERTICE2, Direccion DIRECCION) {
        this.flujoMaximo = flujoMaximo;
        this.VERTICE1 = VERTICE1;
        this.VERTICE2 = VERTICE2;
        this.DIRECCION = DIRECCION;
        this.residual = false;
    }

    /**
     * Constructor para una arista que puede ser residual.
     *
     * @param VERTICE1 Es el primer vértice de la arista.
     * @param VERTICE2 Es el segundo vértice de la arista.
     * @param DIRECCION Es la dirección que tiene la arista.
     * @param residual Bandera que indica si es residual o no.
     */
    public Arista(Vertice VERTICE1, Vertice VERTICE2, Direccion DIRECCION, boolean residual) {
        this.flujoActual = 0;
        this.flujoMaximo = 0;
        this.VERTICE1 = VERTICE1;
        this.VERTICE2 = VERTICE2;
        this.DIRECCION = DIRECCION;
        this.residual = residual;
    }

    /**
     * Método que nos ayuda a identificar si la arista es residual o no.
     *
     * @return true si es residual y false en otro caso.
     */
    public boolean isResidual() {
        return residual;
    }

    /**
     * Método que modifica si una arista es residual o no.
     *
     * @param residual Es la nueva bandera del indicador residual.
     */
    public void setResidual(boolean residual) {
        this.residual = residual;
    }

    /**
     * Método de acceso que regresa el flujo máximo que puede pasar por la
     * arista.
     *
     * @return El flujo máximo de la arista.
     */
    public float getFlujoMaximo() {
        return flujoMaximo;
    }

    /**
     * Método que muta el valor del flujo máximo de la arista.
     *
     * @param flujoMaximo Es el nuevo valor que tendrá el fujo máximo.
     */
    public void setFlujoMaximo(float flujoMaximo) {
        this.flujoMaximo = flujoMaximo;
    }

    /**
     * Método de acceso que obtiene el flujo actual que pasa por la arista.
     *
     * @return El valor del flujo actual.
     */
    public float getFlujoActual() {
        return flujoActual;
    }

    /**
     * Método que muta el valor del flujo actual de la arista.
     *
     * @param flujoActual El nuevo flujo que tendrá la arista.
     */
    public void setFlujoActual(float flujoActual) {
        this.flujoActual = flujoActual;
    }

    /**
     * Método para obtener el hascode de la arista.
     *
     * @return El hascode.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.VERTICE1);
        hash = 89 * hash + Objects.hashCode(this.VERTICE2);
        hash = 89 * hash + Objects.hashCode(this.DIRECCION);
        hash = 89 * hash + (this.residual ? 1 : 0);
        return hash;
    }

    /**
     * Método que compara a la arista con otro objeto para ver si es la misma
     * arista.
     *
     * @param obj Es el objeto a comparar.
     * @return true si son iguales o false en otro caso.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Arista other = (Arista) obj;
        if (this.residual != other.residual) {
            return false;
        }
        if (this.DIRECCION.equals(other.DIRECCION)) {
            if (!Objects.equals(this.VERTICE1, other.VERTICE1)) {
                return false;
            }
            if (!Objects.equals(this.VERTICE2, other.VERTICE2)) {
                return false;
            }
        } else {
            if (!Objects.equals(this.VERTICE1, other.VERTICE2) || !Objects.equals(this.VERTICE2, other.VERTICE1)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Método para obtener la representación en cadena de la arista,
     *
     * @return La representación en cadena de la arista.
     */
    @Override
    public String toString() {
        return "(" + this.VERTICE1.ID + "," + this.VERTICE2.ID
                + ")\nF=" + this.flujoActual + "/" + this.flujoMaximo;
    }

}
