package estructuras;

import enumeraciones.Direccion;
import enumeraciones.TipoVertice;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxStylesheet;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.SimpleDirectedGraph;
import utils.AnalizadorArchivoEntrada;

/**
 * Clase que implementa un red para el problema de Circulaciones con Demanda.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public class Red {

    public final String RUTA_ARCHIVO_ENTRADA, RUTA_DIRECTORIO_SALIDA, NOMBRE_ARCHIVO_ENTRADA;

    public final Set<Vertice> VERTICES, VERTICES_DEMANDA, VERTICES_SUMINISTRO;
    private final Set<Arista> ARISTAS;

    public final Vertice VERTICE_DEMANDA_ESTRELLA, VERTICE_SUMINISTRO_ESTRELLA;

    public final static HashMap<String, Object> ESTILO_VERTICE_DEMANDA,
            ESTILO_VERTICE_DEMANDA_ESTRELLA,
            ESTILO_VERTICE_SUMINISTRO,
            ESTILO_VERTICE_SUMINISTRO_ESTRELLA,
            ESTILO_VERTICE_NORMAL,
            ESTILO_ARISTA;

    /**
     * Método que ocupamos para instanciar todos los estilos para toda instancia
     * de la red.
     */
    static {
        ESTILO_VERTICE_DEMANDA = new HashMap<>();
        ESTILO_VERTICE_DEMANDA.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(255, 128, 128)));
        ESTILO_VERTICE_DEMANDA.put(mxConstants.STYLE_STROKEWIDTH, 1);
        ESTILO_VERTICE_DEMANDA.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);

        ESTILO_VERTICE_SUMINISTRO = new HashMap<>();
        ESTILO_VERTICE_SUMINISTRO.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(192, 255, 128)));
        ESTILO_VERTICE_SUMINISTRO.put(mxConstants.STYLE_STROKEWIDTH, 1);
        ESTILO_VERTICE_SUMINISTRO.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);

        ESTILO_VERTICE_DEMANDA_ESTRELLA = new HashMap<>();
        ESTILO_VERTICE_DEMANDA_ESTRELLA.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_DOUBLE_ELLIPSE);
        ESTILO_VERTICE_DEMANDA_ESTRELLA.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(255, 128, 128)));
        ESTILO_VERTICE_DEMANDA_ESTRELLA.put(mxConstants.STYLE_STROKEWIDTH, 1);
        ESTILO_VERTICE_DEMANDA_ESTRELLA.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);

        ESTILO_VERTICE_SUMINISTRO_ESTRELLA = new HashMap<>();
        ESTILO_VERTICE_SUMINISTRO_ESTRELLA.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_DOUBLE_ELLIPSE);
        ESTILO_VERTICE_SUMINISTRO_ESTRELLA.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_DOUBLE_ELLIPSE);
        ESTILO_VERTICE_SUMINISTRO_ESTRELLA.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(192, 255, 128)));
        ESTILO_VERTICE_SUMINISTRO_ESTRELLA.put(mxConstants.STYLE_STROKEWIDTH, 1);
        ESTILO_VERTICE_SUMINISTRO_ESTRELLA.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);

        ESTILO_VERTICE_NORMAL = new HashMap<>();
        ESTILO_VERTICE_NORMAL.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(new Color(255, 254, 207)));
        ESTILO_VERTICE_NORMAL.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
        ESTILO_VERTICE_NORMAL.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);

        ESTILO_ARISTA = new HashMap<>();
        ESTILO_ARISTA.put(mxConstants.STYLE_FONTCOLOR, mxUtils.getHexColorString(Color.BLACK));
    }

    public Red(String rutaArchivoEntrada, String rutaDirectorioSalida) throws IOException {
        this.RUTA_ARCHIVO_ENTRADA = rutaArchivoEntrada;
        this.RUTA_DIRECTORIO_SALIDA = rutaDirectorioSalida;
        this.VERTICES = new LinkedHashSet<>();
        this.ARISTAS = new LinkedHashSet<>();
        this.VERTICES_DEMANDA = new LinkedHashSet<>();
        this.VERTICES_SUMINISTRO = new LinkedHashSet<>();
        VERTICE_SUMINISTRO_ESTRELLA = new Vertice("s*", TipoVertice.SUMINISTRO_ESTRELLA);
        VERTICE_DEMANDA_ESTRELLA = new Vertice("t*", TipoVertice.DEMANDA_ESTRELLA);
        NOMBRE_ARCHIVO_ENTRADA = new File(RUTA_ARCHIVO_ENTRADA).getName();
        AnalizadorArchivoEntrada.llenaRed(RUTA_ARCHIVO_ENTRADA, this);
    }

    /**
     * Método que añade una nueva arista a la red.
     *
     * @param nueva Es la arista a agregar.
     * @return True si fue posible agregarla o false en otro caso (posiblemente
     * ya está repetida).
     */
    public boolean addArista(Arista nueva) {
        Arista residual = null;
        if (ARISTAS.add(nueva)) {
            switch (nueva.DIRECCION) {
                case DE_V1_A_V2:
                    nueva.VERTICE1.VECINOS.add(nueva);
                    residual = new Arista(nueva.VERTICE2, nueva.VERTICE1, nueva.DIRECCION, true);
                    nueva.VERTICE2.VECINOS.add(residual);
                    break;
            }
            ARISTAS.add(residual);
            return true;
        }
        System.out.println("Advertencia: la arista '("
                + nueva.VERTICE1.ID + "," + nueva.VERTICE2.ID + "," + nueva.DIRECCION + ")' ya existe...");
        return false;
    }

    /**
     * Método que busca un vértice de la red a partir del id del vértice.
     *
     * @param id Es el id del objeto vértice en la gráfica.
     * @return El vértice que tiene ese id o null si no se encuentra.
     */
    public Vertice searchVertice(String id) {
        for (Vertice a : VERTICES) {
            if (a.ID.equals(id)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Método que busca una arista a parir de sus componentes.
     *
     * @param v1 Es el vértice 1 de la arista.
     * @param v2 Es el vértice 2 de la arista.
     * @param d Es la dirección de la arista.
     * @param residual Es la bandera que determina si es vértice residual.
     * @return La arista que coincida con esos criterios o null en otro caso.
     */
    public Arista searchArista(Vertice v1, Vertice v2, Direccion d, boolean residual) {
        Arista busqueda = new Arista(v1, v2, d, residual);
        for (Arista a : ARISTAS) {
            if (a.equals(busqueda)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Método que imprime la gráfica en una imagen.
     *
     * @param nombreGrafica Es el nombre del archivo donde guardaremos la
     * gráfica.
     */
    public void toImage(String nombreGrafica) {

        try {
            SimpleDirectedGraph<Vertice, Arista> g
                    = redToDefaultDirectedGraph();

            JGraphXAdapter<Vertice, Arista> graphAdapter
                    = new JGraphXAdapter<>(g);

            mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
            layout.execute(graphAdapter.getDefaultParent());

            styleGraph(graphAdapter);

            BufferedImage image
                    = mxCellRenderer.createBufferedImage(graphAdapter, null, 5, Color.WHITE, true, null);
            File imgFile = new File(this.RUTA_DIRECTORIO_SALIDA + nombreGrafica);
            ImageIO.write(image, "PNG", imgFile);
            System.out.println("Imagen de la red guardada en '" + imgFile.getAbsolutePath() + "'");
        } catch (IOException ex) {
            System.err.println("No se ha podido generar la imagen de la gráfica.\n"
                    + "Se mostrará la gráfica en modo texto:\n");
        }
        System.out.println("-------------------------------------------\n"
                + this
                + "\n-------------------------------------------");
    }

    /**
     * Método que pone los estilos a la gráfica para que se vea presentable.
     *
     * @param graphAdapter Es el adaptador de la gráfica que nos ayuda a
     * desplegar el objeto tipo Red en una imagen.
     */
    public void styleGraph(JGraphXAdapter<Vertice, Arista> graphAdapter) {
        VERTICES.forEach((v) -> {
            graphAdapter.getVertexToCellMap().get(v).setStyle(v.TIPO_VERTICE.toString());
        });
        ARISTAS.forEach((a) -> {
            if (!a.isResidual()) {
                graphAdapter.getEdgeToCellMap().get(a).setStyle("arista");
            }
        });

        mxStylesheet stylesheet = graphAdapter.getStylesheet();
        stylesheet.putCellStyle(TipoVertice.DEMANDA.toString(), ESTILO_VERTICE_DEMANDA);
        stylesheet.putCellStyle(TipoVertice.SUMINISTRO.toString(), ESTILO_VERTICE_SUMINISTRO);
        stylesheet.putCellStyle(TipoVertice.DEMANDA_ESTRELLA.toString(), ESTILO_VERTICE_DEMANDA_ESTRELLA);
        stylesheet.putCellStyle(TipoVertice.SUMINISTRO_ESTRELLA.toString(), ESTILO_VERTICE_SUMINISTRO_ESTRELLA);
        stylesheet.putCellStyle(TipoVertice.NORMAL.toString(), ESTILO_VERTICE_NORMAL);
        stylesheet.putCellStyle("arista", ESTILO_ARISTA);
    }

    /**
     * Método que deposita los valores del objeto tipo Red a otro objeto de
     * grafos, mismo que se utilizará para desplegar en una imagen.
     *
     * @return El objeto con el formato para mandarlo a imprimir.
     */
    public SimpleDirectedGraph<Vertice, Arista> redToDefaultDirectedGraph() {
        SimpleDirectedGraph<Vertice, Arista> g
                = new SimpleDirectedGraph<>(Arista.class);

        VERTICES.forEach((v) -> {
            g.addVertex(v);
        });

        ARISTAS.forEach((a) -> {
            if (!a.isResidual()) {
                g.addEdge(a.VERTICE1, a.VERTICE2, a);
            }
        });
        return g;
    }

    /**
     * Método que añade a s* y t* a la red, de tal manera que s* se dirigirá a
     * todos los vértices de suministro y todos los vértices de demanda se
     * conectarán con t* (mismos que se ocuparán para el problema de
     * Circulaciones con demanda).
     */
    public void aumenta() {

        this.VERTICES.add(VERTICE_DEMANDA_ESTRELLA);
        this.VERTICES.add(VERTICE_SUMINISTRO_ESTRELLA);
        this.VERTICES_DEMANDA.forEach((v) -> {
            addArista(new Arista(Math.abs(v.getValor()), v, VERTICE_DEMANDA_ESTRELLA, Direccion.DE_V1_A_V2));
        });
        this.VERTICES_SUMINISTRO.forEach((v) -> {
            addArista(new Arista(Math.abs(v.getValor()), VERTICE_SUMINISTRO_ESTRELLA, v, Direccion.DE_V1_A_V2));
        });
    }

    /**
     * Método con el que veremos si podemos ocupar la red para el problema de
     * circulaciones con demanda.
     *
     * @return True si es posible aplicar el algoritmo y false en otro caso.
     */
    public boolean isRedValida() {
        return !this.VERTICES_DEMANDA.isEmpty() && !this.VERTICES_SUMINISTRO.isEmpty();
    }

    public float sumaValoresAristas(Set<Arista> aristas) {
        float res = 0;
        res = aristas.stream().map((a) -> a.getFlujoActual()).reduce(res, (accumulator, _item) -> accumulator + _item);
        return res;
    }

    /**
     * Método que nos ayuda a desplegar la solución del problema de Circulación
     * y Demanda.
     */
    public void imprimeConclusionesProblemaDeCirculacionDemanda() {
        boolean cumplioDemandas = true;
        System.out.println("\n----------------------------\n"
                + "Resultados finales:\n"
                + "-> s*: suministra " + sumaValoresAristas(this.VERTICE_SUMINISTRO_ESTRELLA.VECINOS) + " unidades\n"
                + "-> t*: se depositan " + sumaValoresAristas(this.VERTICE_SUMINISTRO_ESTRELLA.VECINOS) + " unidades\n");
        System.out.println("Vértices de suministro:");
        for (Vertice v : this.VERTICES_SUMINISTRO) {
            float vSuministrado = 0;
            for (Arista a : this.VERTICE_SUMINISTRO_ESTRELLA.VECINOS) {
                if (a.VERTICE2.equals(v)) {
                    vSuministrado = a.getFlujoActual();
                    break;
                }
            }
            System.out.println("-> " + v.ID + " (-" + vSuministrado + "/" + v.getValor() + ")");
        }
        System.out.println("\nVértices de demanda:");
        for (Vertice v : this.VERTICES_DEMANDA) {
            for (Arista a : v.VECINOS) {
                if (a.VERTICE2.TIPO_VERTICE.equals(TipoVertice.DEMANDA_ESTRELLA)) {
                    float difFlujo = v.getValor() - a.getFlujoActual();
                    System.out.println("-> " + v.ID + " (" + a.getFlujoActual() + "/" + v.getValor() + ")" + (difFlujo != 0 ? ", faltó administrar " + (difFlujo) + " u" : ""));
                    if (difFlujo != 0) {
                        cumplioDemandas = false;
                    }
                    break;
                }
            }
        }

        System.out.println("\n" + (cumplioDemandas ? "Todas las demandas se cumplieron" : "Faltaron algunas demandas por cumplir") + ".\n"
                + "----------------------------");
    }

    @Override
    public String toString() {
        String result = "Red{\nVértices:\n";
        for (Vertice v : this.VERTICES) {
            result += "\t-> " + v.ID;
            if (v.TIPO_VERTICE != TipoVertice.NORMAL) {
                result += " - Tipo=[" + v.TIPO_VERTICE + "] - Valor=[" + v.getValor() + "]";
            }
            result += "\n";
        }
        result += "\nAristas:\n";
        for (Arista a : this.ARISTAS) {
            if (a.isResidual()) {
                continue;
            }
            result += "\t-> (" + a.VERTICE1.ID + "," + a.VERTICE2.ID + ") - Flujo=[" + a.getFlujoActual() + "/" + a.getFlujoMaximo() + "]\n";
        }
        return result + "\n}";
    }

}
