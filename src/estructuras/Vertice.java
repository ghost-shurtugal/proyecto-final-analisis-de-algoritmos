package estructuras;

import enumeraciones.TipoVertice;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Clase que implementa a un vertice de una gráfica
 *
 * .* @author Josué Rodrigo Cárdenas Vallarta
 */
public class Vertice {

    public final String ID;
    public final TipoVertice TIPO_VERTICE;
    public final Set<Arista> VECINOS;
    private float valor;

    /**
     * Método constructor básico de un vértice.
     *
     * @param id Es el identificador del vértice.
     * @param tipoVertice Es el tipo de vértice.
     */
    public Vertice(String id, TipoVertice tipoVertice) {
        this.ID = id;
        this.TIPO_VERTICE = tipoVertice;
        this.VECINOS = new LinkedHashSet<>();
    }

    /**
     * Método constructor que se enfoca en crear vértices de suministro o de
     * demanda.
     *
     * @param id Es el identificador del vértice.
     * @param tipoVertice Es el tipo de vértice.
     * @param valor Es el valor que tendrá el vértice.
     */
    public Vertice(String id, TipoVertice tipoVertice, float valor) {
        this(id, tipoVertice);
        this.valor = valor;
    }

    /**
     * Método que obtiene el valor del vértice.
     *
     * @return el valor almacenado en el vértice.
     */
    public float getValor() {
        return valor;
    }

    /**
     * Método de mutación que modifica el valor contenido en el vértice.
     *
     * @param valor Es el nuevo valor del vértice.
     */
    public void setValor(float valor) {
        this.valor = valor;
    }

    /**
     * Método que obtiene a la arista conectada a al vértice que cumpla lo
     * siguiente:
     *
     * 1. Que salga de éste vértice y llegue a otro vértice.
     *
     * 2. Que el vértice destino no sea visitado.
     *
     * 3. Que el flujo pueda pasar por ésta arista.
     *
     * 4. Que la arista a ocupar tenga el mayor flujo posible entre todas las
     * posibles.
     *
     * @param visitados Mapa que contiene los vértices ya visitados.
     * @return La arista que cumpla las características antes mencionadas o null
     * en otro caso.
     */
    public Arista getVecinoConFlujoMaximoPosibleNoVisitado(HashMap<Vertice, Boolean> visitados) {
        Arista res = null;
        float flujoMax = 0;
        for (Arista a : VECINOS) {
            float tmp = a.getFlujoMaximo() - a.getFlujoActual();
            Vertice verticeAlcanzable = a.VERTICE1.equals(this) ? a.VERTICE2 : a.VERTICE1;
            if (!visitados.containsKey(verticeAlcanzable)
                    && tmp > 0 && tmp > flujoMax) {
                flujoMax = tmp;
                res = a;
            }
        }
        return res;
    }

    /**
     * Método que obtiene el hashcode del objeto.
     *
     * @return El valor hash del objeto.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.ID);
        return hash;
    }

    /**
     * Método que compara éste vértice con otro objeto y determina si son los
     * mismos.
     *
     * @param obj El objeto a comparar.
     * @return True si son iguales o false en otro caso.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertice other = (Vertice) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        return true;
    }

    /**
     * Método que obtiene la representación en cadena del vértice.
     *
     * @return La cadena resultante.
     */
    @Override
    public String toString() {
        String res = ID;
        switch (this.TIPO_VERTICE) {
            case DEMANDA:
            case SUMINISTRO:
                res += "\n" + this.TIPO_VERTICE + ": " + this.valor;
                break;
            case DEMANDA_ESTRELLA:
            case SUMINISTRO_ESTRELLA:
                res = " \n     " + res + "    \n ";
                break;
            default:
                res = "    " + res + "    ";
        }
        return res;
    }

}
