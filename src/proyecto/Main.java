package proyecto;

import estructuras.Red;
import java.io.IOException;
import utils.AlgoritmosGrafos;

/**
 * Clase principal para ejecutar el programa.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public class Main {

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                System.err.println("ERROR! - La sintaxis para ejecutar el programa es la siguiente:\n"
                        + "java -jar Proyecto_Final.jar <Archivo Entrada,Directorio Salida>");
                return;
            }
            System.out.println("Obteniendo los datos de la gráfica del archivo '" + args[0] + "'");
            Red entrada = new Red(args[0], args[1] + "/");
            System.out.println("Generando imagen...");
            entrada.toImage("red1-" + entrada.NOMBRE_ARCHIVO_ENTRADA + "-original.png");
            if (entrada.isRedValida()) {
                System.out.println("Obteniendo red aumentada.");
                entrada.aumenta();
                System.out.println("Generando imagen...");
                entrada.toImage("red2-" + entrada.NOMBRE_ARCHIVO_ENTRADA + "-aumentada.png");
                System.out.println("Aplicamos Ford Fulkerson a la red aumentada.");
                AlgoritmosGrafos.fordFulkerson(entrada);
                System.out.println("Generando imagen...");
                entrada.toImage("red3-" + entrada.NOMBRE_ARCHIVO_ENTRADA + "-resultado.png");
                System.out.println("Proceso terminado, los resultados se encuentran en el directorio '" + entrada.RUTA_DIRECTORIO_SALIDA + "' o, en su defecto, en la terminal si no se pudieron generar las imágenes.");
                entrada.imprimeConclusionesProblemaDeCirculacionDemanda();
            } else {
                System.err.println("No se puede ocupar el algoritmo de Circulación y Demanda, falta definir demandas o suministro.");
            }
        } catch (IOException ex) {
            System.err.println("No se pudo guardar el resultado en una imagen.");
        }
    }
}
