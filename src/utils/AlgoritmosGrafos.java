package utils;

import enumeraciones.TipoVertice;
import estructuras.Arista;
import estructuras.Red;
import estructuras.Vertice;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Clase que implementa algunos algoritmos útiles de grafos para el proyecto
 * final.
 *
 * @author Josué Rodrigo Cárdenas Vallarta
 */
public class AlgoritmosGrafos {

    /**
     * Método que ejecuta el algoritmo de Ford Fulkerson en la red.
     *
     * @param red Es la red a aplicarle el algoritmo.
     */
    public static void fordFulkerson(Red red) {
        LinkedList<Arista> camino;
        do {
            camino = DFSModif(red);
            if (camino == null) {
                break;
            }
            float minimo = calcMaximo(camino);
            for (Arista a : camino) {
                Arista inversa = red.searchArista(a.VERTICE2, a.VERTICE1,
                        a.DIRECCION, !a.isResidual());
                if (a.isResidual()) {
                    inversa.setFlujoActual(Math.abs(a.getFlujoActual()) - Math.abs(minimo));
                    a.setFlujoMaximo(Math.abs(inversa.getFlujoMaximo()) - Math.abs(inversa.getFlujoActual()));
                } else {
                    a.setFlujoActual(Math.abs(a.getFlujoActual()) + Math.abs(minimo));
                    inversa.setFlujoMaximo(Math.abs(a.getFlujoMaximo()) - Math.abs(a.getFlujoActual()));
                }
            }
        } while (true);
    }

    /**
     * Método que ejecuta el algoritmo DFS para encontrar un camino de s* a t*.
     *
     * @param red Es la red a evaluar.
     * @return Una lista con las aristas a seguir para ir de s* a t* o null si
     * ya no quedan caminos accesibles.
     */
    public static LinkedList<Arista> DFSModif(Red red) {
        Vertice v = red.VERTICE_SUMINISTRO_ESTRELLA;
        LinkedList<Arista> result = new LinkedList<>();
        HashMap<Vertice, Boolean> visitados = new HashMap<>();
        Stack<Vertice> s = new Stack<>();
        s.push(v);
        visitados.put(v, Boolean.TRUE);
        while (!s.empty()) {
            Vertice actual = s.peek();
            Arista a = actual.getVecinoConFlujoMaximoPosibleNoVisitado(visitados);
            if (a != null) {
                Vertice nuevo;
                if (a.VERTICE1.equals(actual)) {
                    nuevo = a.VERTICE2;
                } else {
                    nuevo = a.VERTICE1;
                }
                visitados.put(nuevo, Boolean.TRUE);
                result.add(a);
                s.push(nuevo);
                if (nuevo.TIPO_VERTICE.equals(TipoVertice.DEMANDA_ESTRELLA)) {
                    return result;
                }
            } else {
                s.pop();
                if (!result.isEmpty()) {
                    result.removeLast();
                }
            }
        }
        return null;
    }

    /**
     * Método auxiliar que ayuda a calcular el flujo máximo que puede pasar en
     * el camino.
     *
     * @param camino Es el camino a evaluar.
     * @return El valor máximo de flujo que puede pasar por el camino.
     */
    private static float calcMaximo(LinkedList<Arista> camino) {
        float max = Float.MAX_VALUE;
        for (Arista a : camino) {
            float flujo = a.getFlujoMaximo() - a.getFlujoActual();
            if (flujo < max) {
                max = flujo;
            }
        }
        return max;
    }
    
}
