package utils;

import estructuras.Vertice;
import estructuras.Arista;
import enumeraciones.Direccion;
import estructuras.Red;
import enumeraciones.TipoVertice;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Clase que contiene métodos para procesar los archivos de entrada.
 *
 * @author Josué Rodrigo Cárdenas Vallarta.
 */
public class AnalizadorArchivoEntrada {

    /**
     * Método que llena un objeto Red a partir de la ruta de un archivo de
     * entrada.
     *
     * @param archivoEntrada Es la ruta de donde se encuentra el archivo de
     * entrada que especifica los vértices y aristas.
     * @param red Es el objeto Red donde se aplicaran los cambios que se
     * encuentren en el archivo de entrada.
     */
    public static void llenaRed(String archivoEntrada, Red red) throws IOException {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    archivoEntrada));
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || line.charAt(0) == '#') {
                    continue;
                }
                addToRed(line, red);
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Se ha detectado un error al momento de leer el archivo '" + archivoEntrada + "'.\n"
                    + "No se pudo generar la gráfica.");
            throw new IOException();
        }
    }

    /**
     * Método auxiliar que, a partir de una línea de texto, se procesan las
     * instrucciones dichas en ella.
     *
     * @param line Es la cadena que tiene las instrucciones para llenar la red.
     * @param red Es el objeto tipo Red que se verá afectado por las
     * instrucciones de la linea.
     * @throws IOException Si se encuentra algún error sintáctico o semántico en
     * el archivo de texto.
     */
    private static void addToRed(String line, Red red) throws IOException {
        try {
            String[] entrada = line.split("\\s+");
            switch (entrada.length) {
                case 3:
                    TipoVertice tipo;
                    switch (entrada[1]) {
                        case "S":
                            tipo = TipoVertice.SUMINISTRO;
                            break;
                        case "D":
                            tipo = TipoVertice.DEMANDA;
                            break;
                        default:
                            System.err.println("Tipo de vertice no reconocido.");
                            throw new IOException();
                    }
                    if (entrada[0].equals("s*") || entrada[0].equals("t*")) {
                        System.err.println("No se puede ocupar el id reservado 's*' p 't*'.");
                        throw new IOException();
                    }
                    Vertice nuevo = new Vertice(entrada[0], tipo, Float.parseFloat(entrada[2]));
                    if (!red.VERTICES.add(nuevo)) {
                        System.out.println("Advertencia: el vertice '" + nuevo.ID + "' ya existe...");
                    } else {
                        switch (nuevo.TIPO_VERTICE) {
                            case SUMINISTRO:
                                if (nuevo.getValor() > 0) {
                                    System.err.println("Se debe ingresar un valor menor o igual a cero en el archivo de entrada.");
                                    throw new IOException();
                                }
                                red.VERTICES_SUMINISTRO.add(nuevo);
                                break;
                            case DEMANDA:
                                red.VERTICES_DEMANDA.add(nuevo);
                                break;
                        }
                    }
                    break;
                case 4:
                    Direccion direccion;
                    switch (entrada[1]) {
                        case "->":
                            direccion = Direccion.DE_V1_A_V2;
                            break;
                        default:
                            System.err.println("Tipo de dirección no reconocida.");
                            throw new IOException();
                    }
                    Vertice vertice1 = red.searchVertice(entrada[0]),
                     vertice2 = red.searchVertice(entrada[2]);
                    if (vertice1 == null) {
                        vertice1 = new Vertice(entrada[0], TipoVertice.NORMAL);
                        red.VERTICES.add(vertice1);
                    }
                    if (vertice2 == null) {
                        vertice2 = new Vertice(entrada[2], TipoVertice.NORMAL);
                        red.VERTICES.add(vertice2);
                    }
                    float flujoMaximo = Float.parseFloat(entrada[3]);
                    if (flujoMaximo < 0) {
                        System.err.println("El flujo máximo definido en un vértice no puede ser menor a cero.");
                        throw new IOException();
                    }
                    red.addArista(new Arista(flujoMaximo, vertice1, vertice2, direccion));
                    break;
                default:
                    throw new IOException();
            }
        } catch (Exception e) {
            System.err.println("Error en la expresión '" + line + "'");
            throw new IOException();
        }
    }

}
